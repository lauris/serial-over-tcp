#!/usr/bin/env python

import sys
import socket
import select

import logging
import time
import threading
import serial
import serial.rfc2217

# TCP server to serve one serial device to multiple remote clients (for.ex. nc or telnet).
#
# Modified code from:
# bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
# github.com/pyserial/pyserial/blob/master/examples/rfc2217_server.py

HOST = "0.0.0.0"
SOCKET_LIST = []
RECV_BUFFER = 4096
ser  = None
r    = None

class Redirector(object):
    def __init__(self, serial_instance, client_sockets, server_socket, debug=True):
        self.serial = serial_instance
        self.client_sockets = client_sockets
        self.server_socket = server_socket
        self._write_lock = threading.Lock()
        self.rfc2217 = serial.rfc2217.PortManager(
            self.serial,
            self,
            logger=logging.getLogger('rfc2217.server') if debug else None)
        self.log = logging.getLogger('redirector')

    def statusline_poller(self):
        self.log.debug('status line poll thread started')
        while self.alive:
            time.sleep(1)
            self.rfc2217.check_modem_lines()
        self.log.debug('status line poll thread terminated')

    def shortcircuit(self):
        """connect the serial port to the TCP port by copying everything
           from one to other"""
        self.alive = True

        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.daemon = True
        self.thread_read.name = 'serial->socket'
        self.thread_read.start()

        self.thread_poll = threading.Thread(target=self.statusline_poller)
        self.thread_poll.daemon = True
        self.thread_poll.name = 'status line poll'
        self.thread_poll.start()

        self.thread_writer = threading.Thread(target=self.writer)
        self.thread_writer.daemon = True
        self.thread_writer.name = 'socket->serial'
        self.thread_writer.start()

    def reader(self):
        """serial -> socket"""
        self.log.debug('reader thread started')

        while self.alive:
            try:
                data = self.serial.read(self.serial.in_waiting or 1)
                if data:
                    # escape outgoing data when needed (Telnet IAC (0xff) character)
                    self.write(b''.join(self.rfc2217.escape(data)))
            except socket.error as msg:
                self.log.error('{}'.format(msg))
                # probably got disconnected
                break

        self.alive = False
        self.log.debug('reader thread terminated')

    def write(self, data):
        """thread safe socket write with no data escaping. used to send telnet stuff"""

        with self._write_lock:
            for s in self.client_sockets:
                if s != self.server_socket:
                    s.sendall(data)

    def writer(self):
        """ socket->serial """
        while self.alive:
            for s in self.client_sockets:
                if s == self.server_socket:
                    continue

                try:
                    data = s.recv(1024)
                    if not data:
                        continue
                    self.serial.write(b''.join(self.rfc2217.filter(data)))
                except socket.error as msg:
                    self.log.error('{}'.format(msg))
                    # probably got disconnected
                    continue
        self.stop()

    def stop(self):
        """Stop copying"""
        self.log.debug('stopping')
        if self.alive:
            self.alive = False
            self.thread_read.join()
            self.thread_poll.join()
            self.thread_writer.join()


def chat_server(serial_device, baudrate, port):
    global SOCKET_LIST
    global ser
    global r

    PORT = port

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)

    # add server socket object to the list of readable connections
    SOCKET_LIST.append(server_socket)

    print "Server is listening on %s:%s" % (HOST, PORT)

    logging.basicConfig(level=logging.INFO)
    logging.getLogger('rfc2217').setLevel(logging.INFO)

    ser = serial.serial_for_url(serial_device, do_not_open=True, baudrate=baudrate)
    ser.timeout = 3     # required so that the reader thread can exit
    # reset control line as no _remote_ "terminal" has been connected yet
    ser.dtr = False
    ser.rts = False

    try:
        ser.open()
    except serial.SerialException as e:
        logging.error("Could not open serial port {}: {}".format(ser.name, e))
        sys.exit(1)

    settings = ser.get_settings()
    ser.rts = True
    ser.dtr = True

    r = Redirector(ser, SOCKET_LIST, server_socket)
    r.shortcircuit()

    while 1:
        # get the list sockets which are ready to be read through select
        # 4th arg, time_out  = 0 : poll and never block
        try:
            ready_to_read, ready_to_write, in_error = select.select(SOCKET_LIST, [], [], 0)
        except KeyboardInterrupt:
            return

        for sock in ready_to_read:
            # a new connection request recieved
            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)

                print "Client from %s:%s connected.." % addr

            # a message from a client, not a new connection
            else:
                # process data recieved from client, 
                try:
                    # receiving data from the socket.
                    data = sock.recv(RECV_BUFFER)
                    if data:
                        # there is something in the socket
                        ser.write(b''.join(r.rfc2217.filter(data)))
                    else:
                        # remove the socket that's broken
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)
                # exception
                except:
                    print "Client (%s, %s) is offline\n" % addr
                    continue

    server_socket.close()

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "Usage example: ./%s /dev/ttyUSB0 115200 8080" % sys.argv[0]
        sys.exit(-1)

    file_name, serial_device, baudrate, port = sys.argv
    sys.exit(chat_server(serial_device, baudrate, int(port)))
