# Serial over TCP

Allow multiple clients use a single serial device over TCP at the same time.

Use nc, telnet or alike to connect to the daemon.

## Caveats:

Rough code, many things could be improved

Clients do not get a proper terminal, so "vi" and "tail -f" will not work

No authentication or other security features what so ever
